#coding=utf-8
'''
Created on 2010-10-12
@author: lxd
'''
import wx
import win32con
import time
import threading
class WorkThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.timeToQuit = threading.Event()
        self.timeToQuit.clear()
        
    def stop(self):
        self.timeToQuit.set()
    def run(self):
        while True:
            if not self.timeToQuit.isSet():
                print 'work'
                time.sleep(1)
            else:
                break
                        
class FrameWithHotKey(wx.Frame):
    def __init__(self, *args, **kwargs):
        wx.Frame.__init__(self, *args, **kwargs)
        self.regHotKey()
        self.Bind(wx.EVT_HOTKEY, self.OnHotKeyStart, id=self.hotKeyId_start)
        self.Bind(wx.EVT_HOTKEY, self.OnHotKeyEnd, id=self.hotKeyId_end)
        self.Bind(wx.EVT_HOTKEY, self.OnHotKeyQuit, id=self.hotKeyId_quit)
        self.work = None
        
    def regHotKey(self):
        self.hotKeyId_start = 100
        self.RegisterHotKey(self.hotKeyId_start, win32con.MOD_ALT, win32con.VK_F1)
        self.hotKeyId_end = 101
        self.RegisterHotKey(self.hotKeyId_end, win32con.MOD_ALT, win32con.VK_F2)
        self.hotKeyId_quit = 102
        self.RegisterHotKey(self.hotKeyId_quit, win32con.MOD_ALT, win32con.VK_F3)
                
    def OnHotKeyStart(self, evt):
        if not self.work:
            self.work = WorkThread()
            self.work.setDaemon(True)            
            self.work.start()
        
    def OnHotKeyEnd(self, evt):
        if self.work:
            self.work.stop()
            self.work = None
            
    def OnHotKeyQuit(self, evt):
        exit()
        
app = wx.App()
FrameWithHotKey(None)
app.MainLoop()




#more
#http://eyehere.net/2012/用python制作游戏外挂（下）/
import ctypes, win32con, ctypes.wintypes, win32gui
EXIT = False
class Hotkey(threading.Thread):
 
    def run(self):
        global EXIT
        user32 = ctypes.windll.user32
        if not user32.RegisterHotKey(None, 99, win32con.MOD_WIN, win32con.VK_F3):
            raise RuntimeError
        try:
            msg = ctypes.wintypes.MSG()
            print msg
            while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
                if msg.message == win32con.WM_HOTKEY:
                    if msg.wParam == 99:
                        EXIT = True
                        return
                user32.TranslateMessage(ctypes.byref(msg))
                user32.DispatchMessageA(ctypes.byref(msg))
        finally:
            user32.UnregisterHotKey(None, 1)
"""
pywin32包能够方便的引入dll并调用其中的api函数，查看MSDN，可以知道RegisterHotKey来自user32.dll文件，它的参数分别是窗口句柄、快捷键标示、组合键和虚拟键。我在消息队列中等待处理事件，如果标示符合，就执行我们自己的代码，否则继续传给系统。具体我也不多解释了，找本Windows编程的书看看就明白了（别找Visual xxx快速入门之类的，那是学龄前孩子看的），记住判断快捷键冲突，程序完事了要释放快捷键就好了。Win + F3退出程序，希望你看代码发现了这个。

这里我把它写成了一个Thread.threading的扩展类，这样可以作为一个线程启动，因为Windows系统在等待按键的时候是阻塞的，那样我们的外挂就不动啦！用一个全局变量来作为按下退出快捷键的标志，这里赞一下Python，虽然不知道是不是标准做法，但是一个global就能让变量在不同线程中共享，实在很方便，有其他语言进行线程编程的童鞋一定深有感触。

便于调试
"""
def TopmostMe():
    hwnd = win32gui.GetForegroundWindow()
    (left, top, right, bottom) = win32gui.GetWindowRect(hwnd)
    win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, left, top, right-left, bottom-top, 0)
"""
这段代码很神奇，运行之后，命令行窗口就总在最前了，这样查看实时的输出就很方便，否则浏览器窗口会阻挡它的。也是很简单的东西，但是我觉得应该有更好的方法，有没有什么方法可以直接给这个窗口设置总在最前的标志而不需要同时设置它的位置？望指教！
"""








#http://www.dev.idv.tw/mediawiki/index.php/Windows系統熱鍵擷取的Python程式範例
#!/usr/bin/env python
# -*- coding: cp950 -*-

# 要使用本範例，您需要先安裝ctypes的模組。此模組可至
# http://starship.python.net/crew/theller/ctypes/取得。

import sys
import time
from ctypes import *
from ctypes.wintypes import *

# 定義一些全域變數以供使用。
# 當熱鍵出現countdown所指定的次數，本程式就會結束。
countdown = 10
# 當連續兩組的熱鍵事件出現，且間隔小於delta所指定的秒數，視為
# double-click的熱鍵。lastTime記錄前一組熱鍵被按下的時間。
delta = 0.5
lastTime = 0

# 以下我們先行定義一些Windows中的常數，以便後面的程式使用。
WM_HOTKEY   = 0x0312
MOD_ALT     = 0x0001
MOD_CONTROL = 0x0002
MOD_SHIFT   = 0x0004

class MSG(Structure):
    """
    此類別用來存放Windows的Message結構。
    """
    _fields_ = [('hwnd', c_int),
                ('message', c_uint),
                ('wParam', c_int),
                ('lParam', c_int),
                ('time', c_int),
                ('pt', POINT)]
# 我們定義的熱鍵為Ctrl-C。當然也可以改成其他的。若要改成Ctrl-Alt-Shift-C，
# 您可以把flag改成MOD_CONTROL | MOD_ALT | MOD_SHIFT
key = ord('C')
flag = MOD_CONTROL
# 我們將透過RegisterHotKey()這個Windows的函式，向系統註冊一個熱鍵。
# 當使用者按下指定的熱鍵時，系統就會送一個WM_HOTKEY的事件給我們。
hotkeyId = 1
if not windll.user32.RegisterHotKey(None, hotkeyId, flag, key):
    sys.exit("無法註冊熱鍵，也酗w被註冊。")

# 我們要自己監看訊息迴圈，看看是不是有WM_HOTKEY的事件給我們。這裡我們也檢查
# 熱鍵是否已經出現指定的次數，如果已經出現指定的次數，就離開訊息迴圈。也就是
# 結束程式。
msg = MSG()
while windll.user32.GetMessageA(byref(msg), None, 0, 0) != 0 and countdown > 0:
    # 若message是WM_HOTKEY就是熱鍵被按下了。
    if msg.message == WM_HOTKEY and msg.wParam == hotkeyId:
        print "Yay!",
        if (time.time() - lastTime) < delta:
            print "A double HotKey."
        else:
            print "A HotKey."
        lastTime = time.time()
        countdown -= 1
    windll.user32.TranslateMessage(byref(msg))
    windll.user32.DispatchMessageA(byref(msg))

# 程式結束