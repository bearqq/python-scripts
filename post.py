import sys, urllib2, urllib

#1
zipcode = sys.argv[1]
url = 'http://www.wunderground.com/cgi-bin/findweather/getForecast'
data = urllib.urlencode([('query', zipcode)])
req = urllib2.Request(url)
fd = urllib2.urlopen(req, data)
print url + data
while 1:
    data = fd.read(1024)
    if not len(data):
        break
    sys.stdout.write(data)
	
	
#2	
url = 'http://umbra.nascom.nasa.gov/cgi-bin/eit-catalog.cgi'
values = {'obs_year':'2011','obs_month':'March',
                             'obs_day':'8','start_year':'2011'
                             ,'start_month':'March','start_day':'8'
                             ,'start_hour':'All Hours','stop_year':'2011'
                             ,'stop_month':'March','stop_day':'8'
                             ,'stop_hour':'All Hours','xsize':'All'
                             ,'ysize':'All','wave':'all'
                             ,'filter':'all','object':'all'
                             ,'xbin':'all','ybin':'all'
                             ,'highc':'all'}
data = urllib.urlencode(values)
print data
req = urllib2.Request(url, data)
response = urllib2.urlopen(req)
the_page = response.read()
print the_page



#3 cookies
cookies = urllib2.HTTPCookieProcessor()
opener = urllib2.build_opener(cookies)

f = opener.open('http://www.ideawu.net/?act=login&name=user01')

data = '<root>Hello</root>'
request = urllib2.Request(
        url     = 'http://www.ideawu.net/?act=send',
        headers = {'Content-Type' : 'text/xml'},
        data    = data)

opener.open(request)
#第一次 open() 是进行登录. 服务器返回的 Cookie 被自动保存在 cookies 中, 被用在后来的请求.
#第二次 open() 用 POST 方法向服务器发送了 Content-Type=text/xml 的数据. 如果你不创建一个 Request, 而是直接使用 urlopen() 方法, Python 强制把 Content-Type 改为 application/x-www-form-urlencoded. 



