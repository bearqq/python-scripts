def send_data_to_wp(title,description,categories,tags):
    '''
    参数说明：
    title：字符串，文章标题
    description：字符串，文章内容
    categories：列表，文章分类名称（可以是一个分类，也可以是多个分类）
    tags：列表，文章tag标签
    如：send_data_to_wp(title="测试文章标题",description="测试文章内容",categories=["测试文章分类1","测试文章分类2"],tags=['测试文章标签1','测试文章标签2'])
    '''
    params={
        'wp_blogid':1,
        'wp_url':'http://localhost/xmlrpc.php',
        'wp_username':'admin',
        'wp_password':'admin',
        }
    server = xmlrpclib.ServerProxy(params['wp_url'])
    data = {'title':title,'description':description,'categories':categories,'mt_keywords':tags}
    status_published=0
    blogs = server.metaWeblog.newPost(params['wp_blogid'],params['wp_username'],params['wp_password'],data,status_published)
    return blogs
	
	
	
import xmlrpclib
blog_username = 'user'
blog_password = 'passwd'
blog_url = 'http://example.com/xmlrpc.php'
have_tags = True
server = xmlrpclib.ServerProxy(blog_url)
push=0
post['title'] = 'Subject'
post['wp_slug'] = 'Slug'
post['wp_author_display_name'] = 'From'
post['categories'] = 'Categories'
if haveTags:
    post['mt_keywords'] = 'Tags'
post['description'] = '\n'.'text'
post['mt_text_more'] = ''
if push:
     post['post_status'] = 'publish'
else:
   post['post_status'] = 'draft'
datetime = 'Date'
strid = 'Post-Id'
if strid == '':
    strid = server.metaWeblog.newPost('', blog_username,blog_password, post,push)