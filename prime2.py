from math import sqrt

def sieve(size):
	#prime=[True]*size
	Sieve = [1 for x in xrange(size)]
	
	rng=xrange
	limit=int(sqrt(size))

	for i in rng(3,limit+1,+2):
		if prime[i]:
			prime[i*i::+i]=[0]*len(prime[i*i::+i])

	return [2]+[i for i in rng(3,size,+2) if prime[i]]

if __name__=='__main__':
	print sieve(1000000000)