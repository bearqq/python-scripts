#!/usr/bin/env python 
# coding: utf-8

#some definitions:
HOST = ''
PORT = 8000

#imports
import socket
import sys

#some global inits:
reload(sys)
sys.setdefaultencoding( "utf-8" )

text_content = '''
HTTP/1.x 200 OK  
Content-Type: text/html

<head>
<title>WOW</title>
</head>
<html>
<p>Wow, Python Server</p>
<IMG src="test.jpg"/>
<form name="input" action="/" method="post">
First name:<input type="text" name="firstname"><br>
<input type="submit" value="Submit">
</form> 
</html>
'''

f = open('test.jpg','rb')
pic_content = '''
HTTP/1.x 200 OK  
Content-Type: image/jpg

'''
pic_content = pic_content + f.read()
f.close()

#def begin
def handleget(src,conn):
	if src == '/test.jpg':
		content = pic_content
	else: content = text_content
	# send message
	conn.sendall(content)

def handlepost(src,entry,conn):
	value = entry[-1].split('=')[-1]
	conn.sendall(text_content + '\n <p>' + value + '</p>')	
	

# Configure socket
def serveron():
	s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((HOST, PORT))
	
	# Serve forever
	while True:
		s.listen(3)
		conn, addr=s.accept()                    
		request=conn.recv(1024)         # 1024 is the receiving buffer size
		method=request.split(' ')[0]
		src=request.split(' ')[1]
	
		print 'Connected by', addr
		print 'Request is:', request
	
		# if GET method request
		if method == 'GET':
			handleget(src,conn)

		# if POST method request
		if method == 'POST':
			form = request.split('\r\n')
			idx = form.index('')             # Find the empty line
			entry = form[idx+1:]               # Main content of the request ['firstname=xx']
			#print entry
			handlepost(src,entry,conn)

		conn.close()

if __name__ == '__main__':
	serveron()