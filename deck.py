# coding: utf-8


def check(n_all,n_out):
	if n_all==0 and n_out==0:
		return 1
	elif n_out<=0:
		return 0
	elif n_all==0:
		return -1
	elif n_out>n_all:
		return 0
	else:
		return -2

def mul(lists):
	m=1
	for l in lists:
		m*=l
	return m

def A(n_all,n_out):
	ck=check(n_all,n_out)
	if ck>=0:
		return ck
	elif ck==-1:
		print 'error ',n_all,n_out
	elif ck==-2:
		return mul([n_all-i for i in range(n_out)])


def C(n_all,n_out):
	ck=check(n_all,n_out)
	if ck>=0:
		return ck
	elif ck==-1:
		print 'error ',n_all,n_out
	elif ck==-2:
		if n_out>(n_all/2):
			n_out=n_all-n_out
		return mul([n_all-i for i in range(n_out)])/A(n_out,n_out)


def P_draw_remain_in_deck1(deck=deck,remain=remain,draw=draw):
	all_goods=0
	for i in range(1,remain+1):
		if i<=draw:
			all_goods+=C(remain,i)*C(deck-remain,draw-i)*A(draw,draw)*A(deck-draw,deck-draw)
			#print all_goods
		else:
			break
	return float(all_goods)/A(deck,deck)

def P_draw_remain_in_deck2(deck=deck,remain=remain,draw=draw):
	return 1-float(A(deck-remain,draw))/A(deck,draw)

deck=rawinput('deck=')
remain=rawinput('remain=')
draw=rawinput('draw=')

print P_draw_remain_in_deck1()
print P_draw_remain_in_deck2()
	
