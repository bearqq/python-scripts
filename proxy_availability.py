#查找1
import urllib2

of = open('proxy.txt' , 'w')
 
for page in range(1, 160):
    html_doc = urllib2.urlopen('http://www.xici.net.co/nn/' + str(page) ).read()
    soup = BeautifulSoup(html_doc)
    trs = soup.find('table', id='ip_list').find_all('tr')
    for tr in trs[1:]:
        tds = tr.find_all('td')
        ip = tds[1].text.strip()
        port = tds[2].text.strip()
        protocol = tds[5].text.strip()
        if protocol == 'HTTP' or protocol == 'HTTPS':
            of.write('%s=%s:%s\n' % (protocol, ip, port) )
            print '%s=%s:%s' % (protocol, ip, port)
 
of.close()

#查找2
import urllib2
from BeautifulSoup import BeautifulSoup
 
# get the proxy
of = open('proxy.txt', 'w')
for page in range(1,50):
    url = 'http://www.xicidaili.com/nn/%s' %page
    user_agent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
    request = urllib2.Request(url)
    request.add_header("User-Agent", user_agent)
    content = urllib2.urlopen(request)
    soup = BeautifulSoup(content)
    trs = soup.find('table', {"id":"ip_list"}).findAll('tr')
    for tr in trs[1:]:
        tds = tr.findAll('td')
        ip = tds[2].text.strip()
        port = tds[3].text.strip()
        protocol = tds[6].text.strip()
        if protocol == 'HTTP' or protocol == 'HTTPS':
            of.write('%s=%s:%s\n' % (protocol, ip, port))
            print '%s://%s:%s' % (protocol, ip, port)

#有代理
import requests
import re
 
of = open('proxy.txt', 'w')
url = 'http://www.youdaili.net/Daili/guonei/3661'
for i in range(1,4):
    if i == 1:
        Url = url+'.html'
    else:
        Url = url+'_%s.html' %i
    html = requests.get(Url).text
    res = re.findall(r'\d+\.\d+\.\d+\.\d+\:\d+', html)
    for pro in res:
        of.write('http=%s\n' %pro)
        print pro
of.closed

#好代理
# -*- coding:utf8 -*-
import requests
import re
import time
from BeautifulSoup import BeautifulSoup
of = open('proxy.txt', 'w')
url = 'http://www.haodailiip.com/guonei/'
for i in range(1,20):
    Url = 'http://www.haodailiip.com/guonei/' + str(i)
    print "正在采集"+Url
    html = requests.get(Url).text
    bs = BeautifulSoup(html)
    table = bs.find('table',{"class":"proxy_table"})
    tr = table.findAll('tr')
    for i in range(1,31):
        td = tr[i].findAll('td')
        proxy_ip = td[0].text.strip()
        proxy_port = td[1].text.strip()
        of.write('http=%s:%s\n' %(proxy_ip,proxy_port))
        print 'http=%s:%s\n' %(proxy_ip,proxy_port)
    time.sleep(2)
of.closed







#验证1
import httplib
import time
import urllib
import threading
 
inFile = open('proxy.txt', 'r')
outFile = open('available.txt', 'w')
 
lock = threading.Lock()
 
def test():
    while True:
        lock.acquire()
        line = inFile.readline().strip()
        lock.release()
        if len(line) == 0: break
        protocol, proxy = line.split('=')
        headers = {'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': ''}
        try:
            conn = httplib.HTTPConnection(proxy, timeout=3.0)
            conn.request(method='POST', url='http://e.meituan.com/m/account/login', body='login=ttttttttttttttttttttttttttttttttttttt&password=bb&remember_username=1&auto_login=1', headers=headers )
            res = conn.getresponse()
            ret_headers = str( res.getheaders() ) 
            html_doc = res.read().decode('utf-8')
            print html_doc.encode('gbk')
            if ret_headers.find(u'/m/account/login/') > 0:
                lock.acquire()
                print 'add proxy', proxy
                outFile.write(proxy + '\n')
                lock.release()
            else:
                print '.',
        except Exception, e:
            print e
 
all_thread = []
for i in range(50):
    t = threading.Thread(target=test)
    all_thread.append(t)
    t.start()
    
for t in all_thread:
    t.join()
 
inFile.close()
outFile.close()

#验证2
#encoding=utf8
import urllib
import socket
socket.setdefaulttimeout(3)
f = open("../src/proxy")
lines = f.readlines()
proxys = []
for i in range(0,len(lines)):
    ip = lines[i].strip("\n").split("\t")
    proxy_host = "http://"+ip[0]+":"+ip[1]
    proxy_temp = {"http":proxy_host}
    proxys.append(proxy_temp)
url = "http://ip.chinaz.com/getip.aspx"
for proxy in proxys:
    try:
        res = urllib.urlopen(url,proxies=proxy).read()
        print res
    except Exception,e:
        print proxy
        print e
        continue




#验证，gevent
# -*- coding: utf-8 -*-
import socket
import ssl
import sys
import urllib
 
import socks
from gevent import Timeout, joinall, monkey, spawn
from gevent.pool import Pool
from gevent.queue import Queue
from sockshandler import SocksiPyHandler
 
import timeme
 
monkey.patch_all()
 
sys.dont_write_bytecode = True
socket.setdefaulttimeout(20)
ssl._create_default_https_context = ssl._create_unverified_context
 
TEST_URL = 'http://baidu.com'
 
 
class ProxyFactory(object):
 
		def __init__(self):
				self.proxy_list = []
 
		def fetch_web_file(self):
				proxy_list = []
				with urllib.request.urlopen('http://proxy.mimvp.com/api/fetch.php?orderid=860160112094625852&num=5000&result_fields=1,2') as file_handler:
						proxy_list = self.parse_proxy_file(file_handler)
				return proxy_list
 
		def parse_proxy_file(self, file_handler=None):
				proxy_list = []
				lines = file_handler.readlines()
				for line in lines:
						if isinstance(line, bytes):
								line = line.decode('ascii')
						line = line.strip()
						proxy_ip, proxy_type = line.split(',')
						proxy_type = proxy_type.lower()
						if proxy_type == 'http/https':
								proxy_type = 'https'
								if proxy_type == 'socks4/socks5':
										proxy_type = 'socks5'
						proxy_url = proxy_type + '://' + proxy_ip
						proxy_list.append(proxy_url)
				return proxy_list
 
		def fetch_local_file(self, name='proxy_list.txt'):
				proxy_list = []
				try:
						with open(name, 'r') as file_handler:
								proxy_list = self.parse_proxy_file(file_handler)
				except (FileNotFoundError) as error:
						print(error)
				return proxy_list
 
		def check_in_sync(self, source_list=[], proxy_filter=[], check_url='http://baidu.com'):
				proxy_list = source_list.copy()
				for proxy in source_list:
						proxy_type, proxy_ip = proxy.split('://')
						if proxy_type in proxy_filter:
								if not self.check_proxy(proxy, check_url):
										proxy_list.remove(proxy)
						else:
								proxy_list.remove(proxy)
				return proxy_list
 
		def check_worker(self, args):
				proxy = args[0]
				check_url = args[1]
				proxy = self.check_proxy(proxy, check_url)
				if proxy and not self.queue.full():
						self.queue.put(proxy)
 
		def check_in_async(self,
											 source_list=[],
											 type_filter_list=['http','https','socks4','socks5'],
											 check_url='http://baidu.com'):
				if source_list and type_filter_list:
						list_size = len(source_list)
						self.pool = Pool(list_size)
						self.queue = Queue(list_size)
 
						proxy_list = []
						for proxy in source_list:
								proxy_type, proxy_ip = proxy.split('://')
								if proxy_type in type_filter_list:
										proxy_list.append(proxy)
 
						self.pool.map(self.check_worker, [(proxy, check_url)
																							for proxy in proxy_list])
						proxy_list = []
						for i in range(self.queue.qsize()):
								proxy_list.append(self.queue.get_nowait())
						return proxy_list
 
		TYPE_MAP = {'socks4': socks.SOCKS4, 'socks5': socks.SOCKS5}
 
		def check_proxy(self, proxy='', url='http://baidu.com'):
				result = False
				if proxy:
						proxy_type, proxy_ip = proxy.split('://')
						print(proxy_type, proxy_ip)
						try:
								proxy_handler = urllib.request.ProxyHandler()
								if proxy_type == 'http' or proxy_type == 'https':
										proxy_handler = urllib.request.ProxyHandler(
												{proxy_type: '%s://%s' % (proxy_type, proxy_ip)})
 
								if proxy_type == 'socks4' or proxy_type == 'socks5':
										(ip, port) = proxy_ip.split(':')
										print(proxy_type, ip, port, url)
										proxy_handler = SocksiPyHandler(self.TYPE_MAP[proxy_type],
																										ip, int(port))
								opener = urllib.request.build_opener(proxy_handler)
								opener.addheaders = [(
										'User-agent',
										'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident'
										'4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)'
								)]
								# urllib.install_opener(opener)
								with timeme.timeme('s', 6) as t_handler:
										with opener.open(url) as f_handler:
												print('%s-%s-%s-%s' %
															(proxy_type, proxy_ip, f_handler.status, url))
								print(t_handler)
								result = True
						except urllib.error.HTTPError as error:
								print(error)
								result = False
						except Exception as error:
								print(error)
								result = False
				if result:
						return proxy
				else:
						return ''
 
 
if __name__ == '__main__':
		url = 'http://taobao.com'
		p = ProxyFactory()
		p_list = p.fetch_web_file()
		temp2 = p.check_in_async(p_list, ['https', 'socks4', 'socks5'], url)
		temp1 = p.check_in_sync(p_list, ['https', 'socks4', 'socks5'], url)
		print(temp1)
		print(temp2)
	
