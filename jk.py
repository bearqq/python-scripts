#coding: utf-8
import os,sys,json,urllib2,cookielib,codecs,time,re
from cStringIO import StringIO
import gzip
from multiprocessing.dummy import Pool

get_all_and_ignore_done=1

if sys.getdefaultencoding() != 'utf8':
    reload(sys)
    sys.setdefaultencoding( "utf-8" )

import ssl
ssl._create_default_https_context = ssl._create_unverified_context

class jk():
    def __init__(self,cookies=''):
        self.cookies=cookies if cookies else "io=WcxfTshPYtjuSsQ4Bg0N; jike:sess.sig=M8tD8Y29noBOraPjdZnTCMNZ9gE; jike:sess=eyJfdWlkIjoiNTkzMjJhZjY2NDk0OWEwMDEyMDNlZTkyIiwiX3Nlc3Npb25Ub2tlbiI6Ijh6NTE0enA2eWlvdmxHeXp2aTl3VEVXenQifQ==;"
        self.total_thread=3
        self.nextfeed="{}"
        self.dir=r'jk/'
        self.done=0
        
    def codeiss(self,data):#decode
        try:
            return data.decode("GB18030")
        except:
            return data.decode("UTF-8")

    def getfeed(self,postdata="{}"):#fetch from url and then decode
        url="https://app.jike.ruguoapp.com/1.0/newsFeed/list"
        request1 = urllib2.Request(url,data=postdata)
        request1.add_header("OS-Version","24")
        request1.add_header("Model","ONEPLUS A3000")
        request1.add_header("Resolution","1080x1920")
        request1.add_header("App-BuildNo","292")
        request1.add_header("Manufacturer","OnePlus")
        request1.add_header("Market","coolapk")
        request1.add_header("OS","android")
        request1.add_header("App-Version","3.2.0")
        request1.add_header("Content-Type","application/json; charset=utf-8")
        request1.add_header("Accept-Encoding","gzip")
        request1.add_header("Cookie",self.cookies)
        request1.add_header("User-Agent","okhttp/3.6.0")
        
        opener1=urllib2.build_opener()
        fdbkf1=opener1.open(request1)

        if  fdbkf1.headers.get('Content-Encoding') == 'gzip':
            feeddata=self.codeiss(gzip.GzipFile(fileobj=StringIO(fdbkf1.read())).read())
        else:
            feeddata=self.codeiss(fdbkf1.read())
        feedjson=json.loads(feeddata)
        return feedjson

    def getindex(self):
        for i in range(5):
            try:
                feedjson=self.getfeed(self.nextfeed)
                break
            except:
                time.sleep(3)
                pass
        else:
            print 'Fetch error'
            sys.exit()
            #return ''
        return feedjson
    
    
    
    def dealindex(self,feedjson):
        def picdownloader(pic):
            url=pic.get('picUrl')
            if url:
                r=re.search('\.com/(.*?)\?',url)
                if r:
                    filename=r.group(1)
                    if '.' not in filename:
                        filename=filename+'.jpg'
                else:
                    if '?' in url:
                        print 'Url re error'
                    filename=url.split('/')[3]
                    if '.' not in filename:
                        filename=filename+'.gif'
                if filename in self.pics:
                    return
                for _ in range(5):
                    try:
                        request1 = urllib2.Request(url)
                        request1.add_header("OS-Version","24")
                        request1.add_header("Model","ONEPLUS A3000")
                        request1.add_header("Resolution","1080x1920")
                        request1.add_header("App-BuildNo","292")
                        request1.add_header("Manufacturer","OnePlus")
                        request1.add_header("Market","coolapk")
                        request1.add_header("OS","android")
                        request1.add_header("App-Version","3.2.0")
                        request1.add_header("Content-Type","application/json; charset=utf-8")
                        request1.add_header("Accept-Encoding","gzip")
                        request1.add_header("User-Agent","okhttp/3.6.0")
                        
                        opener1=urllib2.build_opener()
                        fdbkf1=opener1.open(request1)

                        if  fdbkf1.headers.get('Content-Encoding') == 'gzip':
                            feeddata=gzip.GzipFile(fileobj=StringIO(fdbkf1.read())).read()
                        else:
                            feeddata=fdbkf1.read()
                        break
                    except:
                        time.sleep(3)
                        pass
                else:
                    print "Pic fetch error",url
                    return
                print url
                F=open(workingdir+filename,'wb')
                F.write(feeddata)
                F.close()
                self.pics.append(filename)
                
                
        self.nextfeed=json.dumps({"loadMoreKey":feedjson['loadMoreKey']})
        for i in feedjson['data']:
            if i.get('type')=='MESSAGE':# or i.get('type')=='POPULAR_MESSAGE':
                it=i['item']
                try:
                    workingdir=''.join((self.dir,it.get('id'),'/'))
                    os.mkdir(workingdir)
                    print it.get('id')
                except:
                    print "Done!"
                    if not get_all_and_ignore_done:
                        self.done=1
                    return
                T=codecs.open(''.join((workingdir,it.get('id'),'.txt')),'w','utf-8')
                T.write(it.get('title','NA'))
                T.write('\r\n')
                T.write(it.get('content','NA'))
                if it.get('linkUrl'):
                    T.write('\r\n')
                    T.write(it.get('linkUrl','NA'))
                T.write('\r\n')
                T.write(it.get('updatedAt','NA'))
                video=it.get('video')
                if video:
                    T.write('\r\nVideo:\r\n')
                    T.write(it.get('thumbnailUrl','NA'))
                
                pics=it.get('pictureUrls')
                if pics:
                    T.write('\r\nPICS:\r\n')
                    for pic in pics:
                        T.write(pic.get('picUrl','NA'))
                        T.write('\r\n')
                    pool = Pool(self.total_thread)
                    pool.map(picdownloader,pics)
                    pool.close()
                    pool.join()
                T.close()


    
    def dealexistposts(self):
        try:
            os.mkdir(self.dir)
        except:
            pass
        self.posts=os.listdir(self.dir)
        self.posts=[x for x in self.posts if os.path.isdir(self.dir+x)]
        self.pics=[]
        for p in self.posts:
            files=os.listdir(self.dir+p)
            for f in files:
                if not f.endswith('.txt'):
                    self.pics.append(f)
            
    
    def getall(self):
        self.dealexistposts()
        while not self.done:
            feed=self.getindex()
            self.dealindex(feed)

if __name__ == '__main__':
    j=jk("io=WcxfTshPYtjuSsQ4Bg0N; jike:sess.sig=M8tD8Y29noBOraPjdZnTCMNZ9gE; jike:sess=eyJfdWlkIjoiNTkzMjJhZjY2NDk0OWEwMDEyMDNlZTkyIiwiX3Nlc3Npb25Ub2tlbiI6Ijh6NTE0enA2eWlvdmxHeXp2aTl3VEVXenQifQ==;")
    j.getall()



