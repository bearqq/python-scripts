#1
#! /usr/bin/python2.7
# -- coding:utf-8 --
import os, urllib,urllib2, thread,threading
import re
#匹配音乐url
reg=re.compile('{"name":"(.+?)".+?"rawUrl":"(.+?)",.+?}', re.I)

class downloader(threading.Thread):
	def __init__(self, url, name):
		threading.Thread.__init__(self)
		self.url=url
		self.name=name
	def run(self):
		print 'downloading from %s' % self.url
		urllib.urlretrieve(self.url, self.name)
threads=[]
#多线程下载文件
def main(url):
	response=urllib.urlopen(url)
	text=response.read()
	groups=re.finditer(reg, text)
	for g in groups:
		name=g.group(1).strip() + ".mp3"
		path=g.group(2).replace('\\', '')
		t=downloader(path, name)
		threads.append(t)
		t.start()

if __name__ == '__main__':
	main("http://site.douban.com/huazhou/")
	for t in threads:
		t.join()




#2 more http://xlambda.com/gevent-tutorial/
#! /usr/bin/python2.7
# -- coding:utf-8 --
#可以参考我的博客：http://www.cnblogs.com/descusr
import urllib,urllib2,gevent,re
from gevent import monkey

monkey.patch_all()

def worker(reg, url):
	response=urllib.urlopen(url)
	text=response.read()
	groups=re.finditer(reg, text)
	m_arr = []
	for g in groups:
		name=g.group(1).strip() + ".mp3"
		path=g.group(2).replace('\\', '')
		m_arr.append((name, path))
	return m_arr

#被多线程的函数
def grun(path, name):
	urllib.urlretrieve(path, name)

if __name__ == '__main__':
	#匹配音乐url
	reg=re.compile('{"name":"(.+?)".+?"rawUrl":"(.+?)",.+?}', re.I)    
	musicArray = worker(reg, "http://site.douban.com/huazhou/")
	jobs = []
	for (name, path) in musicArray:
		jobs.append(gevent.spawn(grun, path, name))
	gevent.joinall(jobs)

"""
#libevent 1.4.x
sudo apt-get install libevent-dev

#python_dev
sudo apt-get install python-dev

#easy_install
wget -q http://peak.telecommunity.com/dist/ez_setup.py
sudo python ./ez_setup.py

#greenlet
wget http://pypi.python.org/packages/source/g/greenlet/greenlet-0.3.1.tar.gz#md5=8d75d7f3f659e915e286e1b0fa0e1c4d
tar -xzvf greenlet-0.3.1.tar.gz
cd greenlet-0.3.1/
sudo python setup.py install

#gevent
wget http://pypi.python.org/packages/source/g/gevent/gevent-0.13.6.tar.gz#md5=7c836ce2315d44ba0af6134efbcd38c9
tar -xzvf gevent-0.13.6.tar.gz
cd gevent-0.13.6/
sudo python setup.py install
"""



#3
#一行 Python 实现并行化 -- 日常多线程操作的新思路
#http://blog.segmentfault.com/caspar/1190000000414339?page=1

import urllib2 
#from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool 

urls = [
    'http://www.python.org', 
    'http://www.python.org/about/',
    'http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html',
    'http://www.python.org/doc/',
    'http://www.python.org/download/',
    'http://www.python.org/getit/',
    'http://www.python.org/community/',
    'https://wiki.python.org/moin/',
    'http://planet.python.org/',
    'https://wiki.python.org/moin/LocalUserGroups',
    'http://www.python.org/psf/',
    'http://docs.python.org/devguide/',
    'http://www.python.org/community/awards/'
    # etc.. 
]

# Make the Pool of workers
pool = ThreadPool(4) 
# Open the urls in their own threads
# and return the results
results = pool.map(urllib2.urlopen, urls)
#close the pool and wait for the work to finish 
pool.close() 
<<<<<<< HEAD
pool.join() 




#4现在我们回过头来看文章开始处给出的那段代码：代码中定义了一个函数 threadTest ，它将全局变量逐一的增加 10000 ，然后在主线程中开启了 10 个子线程来调用 threadTest 函数。但结果并不是预料中的 10000 * 10 ，原因主要是对 count 的并发操作引来的。全局变量 count 是共享资源，对它的操作应该串行的进行。
import thread, time, random
count = 0
lock = thread.allocate_lock() #创建一个琐对象
def threadTest():
    global count, lock
    lock.acquire() #获取琐
    
    for i in xrange(10000):
        count += 1
    
    lock.release() #释放琐
for i in xrange(10):
    thread.start_new_thread(threadTest, ())
time.sleep(3)
print count
=======
pool.join() 
>>>>>>> b70d934b0b0916f5f8e801377aedd3276434db27
