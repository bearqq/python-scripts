# encode: utf-8
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
import numpy as np

x1=[]
x2=[]
y1=[]
y2=[]
z1=[]
z2=[]
x3=[]
y3=[]
z3=[]

def update_lines(num, dataLines, lines):
    for line, data in zip(lines, dataLines):
        # NOTE: there is no .set_data() for 3 dim data...
        line.set_data(data[0:2, :num])
        line.set_3d_properties(data[2, :num])
    return lines

# Attaching 3D axis to the figure
fig = plt.figure()
ax = p3.Axes3D(fig)

# Fifty lines of random 3-D lines
#data = [Gen_RandLine(25, 3) for index in range(1)]
data=[np.array([x1,y1,z1])[:,0:1000000:1000],np.array([x2,y2,z2])[:,0:1000000:1000],np.array([x3,y3,z3])[:,0:1000000:1000]]
# Creating fifty line objects.
# NOTE: Can't pass empty arrays into 3d version of plot()
lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0] for dat in data]

# Setting the axes properties
ax.set_xlim3d([-200,200])
ax.set_xlabel('X')

ax.set_ylim3d([-200,200])
ax.set_ylabel('Y')

ax.set_zlim3d([-200,200])
ax.set_zlabel('Z')

ax.set_title('3D Test')

# Creating the Animation object
line_ani = animation.FuncAnimation(fig, update_lines, fargs=(data, lines)  ,interval=.1, blit=False)
line_ani.save('3body2.gif')
plt.show()