
# coding: utf-8

# In[2]:


#code:utf-8
import re,requests,time

phone=input(u"phone:")
mobile=int(phone)
url_blacklist=[]


user_agent = 'User-Agent: Mozilla/5.0 (Linux; Android 7.1.1; MI 6 Build/NMF26X; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/59.0.3071.125 Mobile Safari/537.36 MicroMessenger/6.5.13.1081 NetType/WIFI Language/zh_CN'
headers = {'User-Agent':user_agent,
           'Accept':'*/*',
           'Accept-Encoding':'gzip, deflate, br',
           'Accept-Language':'zh-CN,zh;q=0.8,en;q=0.6',
           'Cache-Control':'no-cache',
           'Connection':'keep-alive',
           'Content-Type':'text/plain;charset=UTF-8'
}

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session
    
s = requests_retry_session()
s.headers.update(headers)


# In[ ]:


def get_pinghongbao_urls():
    r1=s.get('https://www.pinghongbao.com/eleme')
    e1=re.findall(r"(https://www.pinghongbao.com/go/.*?)\"",r1.content.decode('utf-8'))
    return e1

def get_pinghongbao_coupons(url,mobile):
    r2=s.get(url)
    data = {"url":r2.url,"mobile":mobile}
    r3=requests.post('https://hongbao.xxooweb.com/hongbao',data=data)
    print(r3.content.decode('utf-8'))
    if str(data['mobile'])[0:3]+'****'+str(data['mobile'])[-4:] in r3.content.decode('utf-8'):
        print('已领pinghongbao')
        return 1
    return 0

def pinghongbao():
    urls=get_pinghongbao_urls()
    for url in urls:
        if url not in url_blacklist:
            url_blacklist.append(url)
            r=get_pinghongbao_coupons(url,mobile)
            if r:
                return 1

def get_qfqnet_urls():
    r1=s.get('https://share.qfqnet.com/coupon/2')
    e1=re.findall(r'"(/coupon/redirect/.*?)"',r1.content.decode('utf-8'))
    return e1

def get_qfqnet_coupons(url,mobile):
    r2=s.get('https://share.qfqnet.com'+url)
    data = {"url":r2.url,"mobile":mobile}
    r3=requests.post('https://hongbao.xxooweb.com/hongbao',data=data)
    print(r3.content.decode('utf-8'))
    if str(data['mobile'])[0:3]+'****'+str(data['mobile'])[-4:] in r3.content.decode('utf-8'):
        print('已领qfqnet')
        return 1
    return 0

def qfqnet():
    urls=get_qfqnet_urls()
    for url in urls:
        if url not in url_blacklist:
            url_blacklist.append(url)
            r=get_qfqnet_coupons(url,mobile)
            if r:
                return 1

while 1:
    try:
        if not qfqnet():
            if not pinghongbao():
                print('获取失败')
                time.sleep(60)
                continue
        break
    except Exception as e:
        print(e)
    time.sleep(60)
    

