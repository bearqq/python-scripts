#--coding:utf-8--
from multiprocessing.dummy import Pool
pool=Pool(4)

a=-10.0/1000. #积分上限
b=0.0/1000. #积分下限
width=0.1/1000.

def func(x): 
    """
    定义被积分函数
    """
    x=x*1000
    return 1/(0.0036*x+0.0421)


def Get_N(a,b,width):
    # width为步长
    N=int((b-a)/width + 1)
    if N%2 == 0:
        N=N+1
    return N

def GenerateData(a,b,n,width):
    #r=[a+i*width for i in xrange(0,n)]
    def r():
        for i in xrange(0,n):
            yield a+i*width
    r=r()
    return pool.map(func,r)
    """
    datas = []
    r=a
    for i in range(0,n):
        datas.append(func(r))
        r = r+width
    return datas
    """

def simpson_integral(datas,width,n):
    sum = datas[0]+datas[n-1]
    for i in range(2,n):
        if i%2== 0:
            sum = sum +4*datas[i-1]
        else:
            sum = sum +2*datas[i-1]
    return sum*width/3.0
 
 
if __name__ == "__main__":
    N=Get_N(a,b,width)
    datas = GenerateData(a,b,N,width)
    print simpson_integral(datas,width,N)
     
    #调用scipy.integrate 包进行计算
    import scipy.integrate as integrate
    print integrate.quad(func, a, b)[0]